#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int MAX_BUFFER_SIZE = 1024;
int PORT = 5566;

int main (int argc, char* argv[]) {
    char *username = NULL, *password = NULL;

    if (argc != 6) {
        exit(1);
    } else {
        if (!strcmp(argv[1], "-u")) username = strdup(argv[2]);
        if (!strcmp(argv[3], "-p")) password = strdup(argv[4]);
    }

    char *database = malloc(32);
    strcpy(database, argv[5]);

    char *ip = "127.0.0.1";

    int sock;
    struct sockaddr_in addr;
    char buffer[MAX_BUFFER_SIZE];

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0){
        perror("[-]Socket error");
        exit(1);
    }

    memset(&addr, '\0', sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = PORT;
    addr.sin_addr.s_addr = inet_addr(ip);

    if(connect(sock, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        exit(1);
    }

    if (!getuid()) {
        username = strdup("root");
        password = strdup("root");
    } else if (username == NULL || password == NULL) {
        exit(1);
    }

    // START LOGGING IN
    bzero(buffer, MAX_BUFFER_SIZE);
    strcpy(buffer, username);
    strcat(buffer, " ");
    strcat(buffer, password);
    send(sock, buffer, MAX_BUFFER_SIZE, 0);

    bzero(buffer, MAX_BUFFER_SIZE);
    recv(sock, buffer, sizeof(buffer), 0);

    if (strcmp(buffer, "SUCCESS") != 0) {
        exit(1);
    }
    // FINISH LOGGING IN

    // USE database
    bzero(buffer, MAX_BUFFER_SIZE);
    strcpy(buffer, "USE,");
    strcat(buffer, database);
    send(sock, buffer, MAX_BUFFER_SIZE, 0);

    bzero(buffer, MAX_BUFFER_SIZE);
    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

    if (strcmp(buffer, "SUCCESS") != 0) exit(1);
    // FINISH USE database

    // DUMP
    bzero(buffer, MAX_BUFFER_SIZE);
    strcpy(buffer, "DUMP,");
    send(sock, buffer, MAX_BUFFER_SIZE, 0);

    bzero(buffer, MAX_BUFFER_SIZE);
    recv(sock, buffer, MAX_BUFFER_SIZE, 0);
    puts(buffer);
    // FINSIH DUMP
}