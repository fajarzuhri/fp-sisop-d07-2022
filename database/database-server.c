#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/stat.h>

char * DEFAULT_DATABASE_NAME = "default";
int PORT = 5566;
int MAX_BUFFER_SIZE = 1024;

char *ltrim(char *s, char c) {
    while(*s == c) s++;
    return s;
}

char *rtrim(char *s, char c) {
    char* back = s + strlen(s);
    while(*--back == c);
    *(back+1) = '\0';
    return s;
}

char *trim(char *s, char c) {
    return rtrim(ltrim(s, c), c);
}

void *connection_handler(void *socket_desc);

int main(){
    int server_sock, client_sock, *new_sock;
    struct sockaddr_in server_addr, client_addr;
    socklen_t addr_size;
    int n;

    server_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (server_sock < 0) {
        perror("[-]Socket error");
        exit(1);
    }
    printf("[+]TCP server socket created.\n");

    memset(&server_addr, '\0', sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = PORT;
    server_addr.sin_addr.s_addr = INADDR_ANY;

    n = bind(server_sock, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if (n < 0) {
        perror("[-]Bind error");
        exit(1);
    }
    printf("[+]Bind to the port number: %d\n", PORT);

    listen(server_sock, 5);
    printf("Listening...\n");

    while((client_sock = accept(server_sock, (struct sockaddr *)&client_addr, (socklen_t*)&addr_size))) {
        puts("Connection accepted");

        pthread_t sniffer_thread;
        new_sock = (int*) malloc(1);
        *new_sock = client_sock;

        if(pthread_create(&sniffer_thread ,NULL ,connection_handler,(void*) new_sock) != 0) {
            perror("THREAD ERROR");
        }
    }

    if (client_sock < 0) perror("CLIENT SOCKET ERROR");

    return 0;
}

char * getNumOfColumn(char* path, char* columnName) {
    char command[1024], line[128], *output = malloc(2);
    bzero(command, 1024);
    bzero(line, 128);
    sprintf(command, "head -1 %s | sed 's/|/\\n/g' | nl | awk '$2 ~ /^%s-/ {print $1}'", path, columnName);
    puts(command);

    FILE *fpipe;
    if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);

    if (fgets(line, 128, fpipe) != NULL) {
        strcpy(output, rtrim(line, '\n'));
        pclose(fpipe);
        return output;
    }

    pclose(fpipe);
    return NULL;
}


void *connection_handler(void *socket_desc) {
    // APP STATES;
    char currentUser[32];
    char currentDatabase[32];
    strcpy(currentDatabase, DEFAULT_DATABASE_NAME);

    //Get the socket descriptor
    int client_sock = *(int *) socket_desc;
    ssize_t read_size;
    char buffer[MAX_BUFFER_SIZE], command[MAX_BUFFER_SIZE], query[128], log[128];

    // START LOGGING IN
    puts("HELLO");
    bzero(buffer, MAX_BUFFER_SIZE);
    recv(client_sock, buffer, sizeof(buffer), 0);

    bzero(command, MAX_BUFFER_SIZE);
    strcpy(command, "awk \"/^");
    strcat(command, buffer);
    strcat(command, "$/\" ./databases/default/users");

    FILE *fpipe;
    char output[MAX_BUFFER_SIZE], line[128];
    bzero(output, MAX_BUFFER_SIZE);
    bzero(line, 128);

    if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);

    if (!strcmp(buffer, "root root")) {
        bzero(buffer, MAX_BUFFER_SIZE);
        strcpy(currentUser, "root");
        strcpy(buffer, "SUCCESS");
    } else if (fgets(line, 128, fpipe) != NULL) {
        bzero(buffer, MAX_BUFFER_SIZE);
        strcpy(currentUser, strtok(line, " "));
        strcpy(buffer, "SUCCESS");
    } else {
        bzero(buffer, MAX_BUFFER_SIZE);
        strcpy(buffer, "ERROR");
    }
    send(client_sock, buffer, 1024, 0);
    puts("HMMMM");
    // FINISH LOGGING IN

    while (1) {
        char logtime[32];
        time_t rawtime = time(NULL);
        struct tm * timeinfo;
        timeinfo = localtime(&rawtime);
        strftime(logtime,32, "%Y-%m-%d %H:%M:%S", timeinfo);

        bzero(buffer, MAX_BUFFER_SIZE);
        if ((read_size = recv(client_sock, buffer, MAX_BUFFER_SIZE, 0)) <= 0) break;

        puts(buffer);
        // GETTING MAIN COMMAND
        strcpy(buffer, strtok(buffer, "~"));
        strcpy(query, strtok(NULL, "~"));
        strcpy(buffer, strtok(buffer, ","));

        sprintf(log, "%s:%s:%s", logtime, currentUser, query);

        FILE *logFile;
        logFile = fopen("database-server.log", "a");

        if (logFile == NULL) send(client_sock, "ERROR : logging", MAX_BUFFER_SIZE, 0);

        fprintf(logFile, "%s", log);
        fclose(logFile);

        puts(log);

        if(!strcmp(buffer, "USE")) {
            char *databaseName, *path = (char *) malloc(1024);
            databaseName = strtok(NULL, ",");
            strcpy(path, "databases/");
            strcat(path, databaseName);
            DIR* dir = opendir(path);

            if (dir) {
                int allowed = 0;
                if (!strcmp(currentUser, "root")) allowed = 1;
                else {
                    bzero(command, MAX_BUFFER_SIZE);
                    strcpy(command, "awk \"/^");
                    strcat(command, databaseName);
                    strcat(command, " ");
                    strcat(command, currentUser);
                    strcat(command, "$/\" ./databases/default/access_list");

                    if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);
                    if (fgets(line, 128, fpipe) != NULL) allowed = 1;

                    pclose(fpipe);
                }

                if (allowed) {
                    strcpy(currentDatabase, databaseName);
                    send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                } else {
                    send(client_sock, "ERROR : you dont have access to this database", MAX_BUFFER_SIZE, 0);
                }

            } else {
                send(client_sock, "ERROR : database not found", MAX_BUFFER_SIZE, 0);
            }
        } else if (!strcmp(buffer, "CREATE USER")) {
            if (!strcmp(currentUser, "root")) {
                char *username = strtok(NULL, ",");
                char *password = strtok(NULL, ",");

                bzero(command, MAX_BUFFER_SIZE);
                strcpy(command, "awk \"/^");
                strcat(command, username);
                strcat(command, " ");
                strcat(command, "/\" ./databases/default/users");

                if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);
                if (fgets(line, 128, fpipe) != NULL) {
                    bzero(buffer, MAX_BUFFER_SIZE);
                    strcpy(buffer, "ERROR : user ");
                    strcat(buffer, username);
                    strcat(buffer, " already exist");
                    send(client_sock, buffer, MAX_BUFFER_SIZE, 0);
                } else {
                    char *path = "databases/default/users";
                    FILE *userDb;
                    userDb = fopen(path, "a");
                    if (userDb == NULL) {
                        send(client_sock, "ERROR : db cannot be accessed", MAX_BUFFER_SIZE, 0);
                        continue;
                    }
                    fprintf(userDb, "%s %s\n", username, password);
                    fclose(userDb);
                    send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                }
                pclose(fpipe);
            } else {
                send(client_sock, "ERROR : You don't have permission to create user. Log in as root to create user", MAX_BUFFER_SIZE, 0);
            }
        } else if (!strcmp(buffer, "GRANT PERMISSION")) {
            if (!strcmp(currentUser, "root")) {
                char *databaseName = strtok(NULL, ",");
                char *username = strtok(NULL, ",");

                bzero(command, MAX_BUFFER_SIZE);
                strcpy(command, "awk \"/^");
                strcat(command, username);
                strcat(command, " ");
                strcat(command, "/\" ./databases/default/users");

                if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);
                if (fgets(line, 128, fpipe) == NULL) {
                    send(client_sock, "ERROR : user didnt exist", MAX_BUFFER_SIZE, 0);
                    continue;
                }

                pclose(fpipe);

                char *path = (char *) malloc(1024);
                strcpy(path, "databases/");
                strcat(path, databaseName);
                DIR* dir = opendir(path);

                if (dir) {
                    bzero(command, MAX_BUFFER_SIZE);
                    strcpy(command, "awk \"/^");
                    strcat(command, databaseName);
                    strcat(command, " ");
                    strcat(command, username);
                    strcat(command, "$/\" ./databases/default/access_list");

                    if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);
                    if (fgets(line, 128, fpipe) != NULL) {
                        send(client_sock, "ERROR : user already have permission to the database", MAX_BUFFER_SIZE, 0);
                        continue;
                    }

                    pclose(fpipe);

                    FILE *file;
                    file = fopen("databases/default/access_list", "a");
                    fprintf(file, "%s %s\n", databaseName, username);
                    fclose(file);

                    send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                } else {
                    send(client_sock, "ERROR : database didn't exist", MAX_BUFFER_SIZE, 0);
                }
            } else {
                send(client_sock, "ERROR : You don't have permission to grant permission. Log in as root to grant permission", MAX_BUFFER_SIZE, 0);
            }
        } else if (!strcmp(buffer, "CREATE DATABASE")) {
            char *databaseName = strtok(NULL, ",");

            char *path = (char *) malloc(1024);
            strcpy(path, "databases/");
            strcat(path, databaseName);
            DIR* dir = opendir(path);

            if (dir) {
                send(client_sock, "ERROR : database already exist", MAX_BUFFER_SIZE, 0);
            } else {
                bzero(command, MAX_BUFFER_SIZE);
                strcpy(command, "mkdir ");
                strcat(command, path);
                system(command);

                if (strcmp(currentUser, "root") != 0) {
                    FILE *accessListDb;
                    accessListDb = fopen("databases/default/access_list", "a");
                    if (accessListDb == NULL) {
                        send(client_sock, "ERROR : db cannot be accessed", MAX_BUFFER_SIZE, 0);
                        continue;
                    }
                    fprintf(accessListDb, "%s %s\n", databaseName, currentUser);
                    fclose(accessListDb);
                }
                send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
            }
        } else if (!strcmp(buffer, "CREATE TABLE")) {
            if (!strcmp(currentDatabase, "default")) {
                send(client_sock, "ERROR : cannot create table on database default", MAX_BUFFER_SIZE, 0);
                continue;
            }

            char *tableName = strtok(NULL, ",");
            char *path = (char *) malloc(1024);
            strcpy(path, "databases/");
            strcat(path, currentDatabase);
            strcat(path, "/");
            strcat(path, tableName);

            if (!access(path, F_OK)) {
                send(client_sock, "ERROR : table already exist", MAX_BUFFER_SIZE, 0);
            } else {
                bzero(command, MAX_BUFFER_SIZE);
                strcpy(command, "touch ");
                strcat(command, path);

                system(command);

                FILE *table;
                table = fopen(path, "w");
                if (table == NULL) {
                    send(client_sock, "ERROR : table cannot be created", MAX_BUFFER_SIZE, 0);
                    continue;
                }

                char *column = strtok(NULL, ",");
                while (column != NULL) {
                    char *end;
                    fprintf(table, "%s", column);
                    column = strtok(NULL, ",");
                    if (column != NULL) fprintf(table, "|");
                }
                fprintf(table, "\n");
                fclose(table);

                send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
            }
        } else if (!strcmp(buffer, "DROP DATABASE")) {
            char *databaseName = strtok(NULL, ",");

            char *path = (char *) malloc(1024);
            strcpy(path, "databases/");
            strcat(path, databaseName);
            DIR* dir = opendir(path);

            if (dir) {
                int allowed = 0;

                if (!strcmp(currentUser, "root")) allowed = 1;

                bzero(command, MAX_BUFFER_SIZE);
                strcpy(command, "awk \"/^");
                strcat(command, databaseName);
                strcat(command, " ");
                strcat(command, currentUser);
                strcat(command, "$/\" ./databases/default/access_list");

                if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);
                if (fgets(line, 128, fpipe) != NULL) allowed = 1;

                pclose(fpipe);

                if (allowed) {
                    bzero(path, 1024);
                    strcpy(path, "databases/");
                    strcat(path, databaseName);

                    bzero(command, MAX_BUFFER_SIZE);
                    strcpy(command, "rm -r ");
                    strcat(command, path);

                    if (!system(command)) {

                        bzero(command, MAX_BUFFER_SIZE);
                        strcpy(command, "awk \"!/^");
                        strcat(command, databaseName);
                        strcat(command, " ");
                        strcat(command, "/\" ./databases/default/access_list > myfile ");
                        strcat(command, "&& mv myfile databases/default/access_list");
                        system(command);

                        send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                    } else {
                        send(client_sock, "ERROR : cannot delete database", MAX_BUFFER_SIZE, 0);
                    }
                } else {
                    send(client_sock, "ERROR : you dont have permission to drop this database", MAX_BUFFER_SIZE, 0);
                }

            } else {
                send(client_sock, "ERROR : database didn't exist", MAX_BUFFER_SIZE, 0);
            }
        } else if (!strcmp(buffer, "DROP TABLE")) {
            char *tableName = strtok(NULL, ",");

            char *path = (char *) malloc(1024);
            strcpy(path, "databases/");
            strcat(path, currentDatabase);
            strcat(path, "/");
            strcat(path, tableName);

            if (access(path, F_OK)) {
                send(client_sock, "ERROR : table didn't exist", MAX_BUFFER_SIZE, 0);
            } else {
                bzero(command, MAX_BUFFER_SIZE);
                strcpy(command, "rm ");
                strcat(command, path);

                if (!system(command)) {
                    send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                } else {
                    send(client_sock, "ERROR : cannot drop table", MAX_BUFFER_SIZE, 0);
                }
            }
        } else if (!strcmp(buffer, "DROP COLUMN")) {
            char *columnName = strtok(NULL, ",");
            char *tableName = strtok(NULL, ",");

            char *path = (char *) malloc(1024);
            strcpy(path, "databases/");
            strcat(path, currentDatabase);
            strcat(path, "/");
            strcat(path, tableName);

            if (access(path, F_OK)) {
                send(client_sock, "ERROR : table didn't exist", MAX_BUFFER_SIZE, 0);
            } else {
                char *nColumn = getNumOfColumn(path, columnName);
//                bzero(command, MAX_BUFFER_SIZE);
//                strcpy(command, "head -1  ");
//                strcat(command, path);
//                strcat(command, " | sed 's/|/\\n/g' | nl | awk '/");
//                strcat(command, columnName);
//                strcat(command, "/ {print $1}'");
//
//                if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);
                if (nColumn != NULL) {
                    bzero(command, MAX_BUFFER_SIZE);
                    sprintf(command, "awk -F \"|\" -v e=%s '{for(i=1;i<=NF;i++) if(i!=e) "
                                     "printf(\"%%s%%s\",$i,(i==NF||(i==NF-1&&e==NF))?\"\\n\":\"|\")}' %s "
                                     "> tmp && mv tmp %s", nColumn, path, path);
//                    strcpy(command, "awk -F \"|\" -v e=");
//                    strcat(command, nColumn);
//                    strcat(command, " '{for(i=1;i<=NF;i++) if(i!=e) printf(\"%s%s\",$i,(i==NF||(i==NF-1&&e==NF))?\"\\n\":\"|\")}' ");
//                    strcat(command, path);
//                    strcat(command, " > tmp && mv tmp ");
//                    strcat(command, path);

                    if (!system(command)) {
                        send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                    } else {
                        send(client_sock, "ERROR : cannot drop column from the table", MAX_BUFFER_SIZE, 0);
                    }
                } else {
                    send(client_sock, "ERROR : column didn't exist on the table", MAX_BUFFER_SIZE, 0);
                }

            }

        } else if (!strcmp(buffer, "INSERT INTO")) {
            char *tableName = strtok(NULL, ",");
            char *path = (char *) malloc(1024);
            sprintf(path, "databases/%s/%s", currentDatabase, tableName);

            FILE *table;
            table = fopen(path, "a");

            if (table == NULL) {
                send(client_sock, "ERROR : table didn't exist", MAX_BUFFER_SIZE, 0);
                continue;
            }

            bzero(command, MAX_BUFFER_SIZE);
            sprintf(command, "head -1 %s | awk -F \"|\" '{print NF}'", path);
            puts(command);

            if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);
            if (fgets(line, 128, fpipe) != NULL) {
                int numOfCol = atoi(rtrim(line, '\n'));
                printf("%d\n", numOfCol);
                if (numOfCol) {
                    char *columns = malloc(1024), columnInput[1024];
                    memset(columnInput, '\0', 1024);
                    strcpy(columnInput, strtok(NULL, ""));

                    int colIndex = 0, nCol = 0;
                    char *currentPos = &columnInput[colIndex], nextChar;

                    while(*currentPos != '\0') {
                        if (*currentPos == ' ') currentPos++;
                        if (*currentPos == '\'') nextChar = '\'';
                        else nextChar = ',';

                        char *x = strchr(currentPos+1, nextChar);
                        if (x != NULL && *(x+1) != '\0') {
                            strncat(columns, currentPos, x - currentPos + (*currentPos == '\''));
                            strcat(columns, "|");


                            colIndex = x - currentPos + 1 + (*currentPos == '\'');
                            currentPos += colIndex;
                        } else {
                            strcat(columns, currentPos);
                            currentPos += strlen(currentPos);
                        }
                        nCol++;
                    }

                    if (nCol != numOfCol) {
                        send(client_sock, "ERROR : invalid number of column", MAX_BUFFER_SIZE, 0);
                        continue;
                    }

                    fprintf(table, "%s\n", columns);
                    fclose(table);
                    send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                } else {
                    send(client_sock, "ERROR : error get number of column", MAX_BUFFER_SIZE, 0);
                }
            } else {
                send(client_sock, "ERROR : error get number of column", MAX_BUFFER_SIZE, 0);
            }
        } else if (!strcmp(buffer, "UPDATE")) {
            char *tableName = strtok(NULL, "|");
            char *columnName = strtok(NULL, "|");
            char *newValue = strtok(NULL, "|");
            char *newNew = malloc(32);
            if (*newValue == '\'') {
                sprintf(newNew, "%s%s%s", "'\\'", newValue, "\\''");
            } else {
                strcpy(newNew, newValue);
            }

            char *columnComp = strtok(NULL, "|");
            char *valueComp = columnComp != NULL ? strtok(NULL, "|") : NULL;
            char *newValComp = malloc(32);
            if (valueComp != NULL) {
                if (*valueComp == '\'') {
                    sprintf(newValComp, "%s%s%s", "'\\'", valueComp, "\\''");
                } else {
                    strcpy(newValComp, valueComp);
                }
            }

            char *path = (char *) malloc(1024);
            sprintf(path, "databases/%s/%s", currentDatabase, tableName);

            if (access(path, F_OK)) {
                send(client_sock, "ERROR : table didn't exist", MAX_BUFFER_SIZE, 0);
            } else {
                char *nColumn = getNumOfColumn(path, columnName);

                if (nColumn != NULL) {
                    puts(nColumn);
                    if (columnComp != NULL && valueComp != NULL) { // PAKE WHERE
                        char *nColumnComp = getNumOfColumn(path, columnComp);
                        getNumOfColumn(path, "col3");

                        if (nColumnComp != NULL) {
                            puts(nColumn);
                            puts(nColumnComp);
                            bzero(command, MAX_BUFFER_SIZE);
                            sprintf(command, "awk -F \"|\" '{for(i=0;i<NF;i++) if($%s==\"%s\") $%s=\"%s\"} {print}' OFS=\"|\" %s > tmp && mv tmp %s",
                                    nColumnComp, newValComp,
                                    nColumn, newNew,
                                    path, path);
                            puts(nColumn);
                            puts(nColumnComp);
                            puts(command);
                            if (system(command)) {
                                send(client_sock, "ERROR : awk error", MAX_BUFFER_SIZE, 0);
                            } else {
                                send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                            }
                        } else {
                            send(client_sock, "ERROR : column didn't exist on the table", MAX_BUFFER_SIZE, 0);
                        }
                    } else { // GAPAKE WHERE
                        bzero(command, MAX_BUFFER_SIZE);
                        sprintf(command, "awk -F \"|\" 'NR > 1{$%s=\"%s\"} {print}' OFS=\"|\" %s > tmp && mv tmp %s", nColumn, newNew, path, path);
                        puts(command);
                        if (system(command)) {
                            send(client_sock, "ERROR : awk error", MAX_BUFFER_SIZE, 0);
                        } else {
                            send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                        }
                    }
                } else {
                    send(client_sock, "ERROR : column didn't exist on the table", MAX_BUFFER_SIZE, 0);
                }
            }
        } else if (!strcmp(buffer, "DELETE FROM")) {
            char *tableName = strtok(NULL, "|");
            char *column = strtok(NULL, "|");
            char *value = column != NULL ? strtok(NULL, "|") : NULL;
            char *newValue = malloc(32);
            if (value != NULL) {
                if (*value == '\'') {
                    sprintf(newValue, "%s%s%s", "'\\'", value, "\\''");
                } else {
                    strcpy(newValue, value);
                }
            }

            char *path = (char *) malloc(1024);
            sprintf(path, "databases/%s/%s", currentDatabase, tableName);

            if (access(path, F_OK)) {
                send(client_sock, "ERROR : table didn't exist", MAX_BUFFER_SIZE, 0);
            } else {
                if (column != NULL) {
                    char *nColumnComp = getNumOfColumn(path, column);

                    if (nColumnComp != NULL) {
                        bzero(command, MAX_BUFFER_SIZE);
                        sprintf(command, "awk -F \"|\" '!($%s==\"%s\")' %s > tmp && mv tmp %s", nColumnComp, newValue, path, path);
                        puts(command);

                        if (system(command)) {
                            send(client_sock, "ERROR : awk error", MAX_BUFFER_SIZE, 0);
                        } else {
                            send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                        }
                    } else {
                        send(client_sock, "ERROR : column didn't exist on the table", MAX_BUFFER_SIZE, 0);
                    }
                } else {
                    puts("masuk situ");
                    bzero(command, MAX_BUFFER_SIZE);
                    sprintf(command, "awk -F \"|\" 'NR==1' %s > tmp && mv tmp %s", path, path);

                    if (system(command)) {
                        send(client_sock, "ERROR : awk error", MAX_BUFFER_SIZE, 0);
                    } else {
                        send(client_sock, "SUCCESS", MAX_BUFFER_SIZE, 0);
                    }
                }
            }
        } else if (!strcmp(buffer, "SELECT")) {
            char *columns = strtok(NULL, "|");
            char *tableName = strtok(NULL, "|");
            char *columnComp = strtok(NULL, "|");
            char *value = columnComp != NULL ? strtok(NULL, "|") : NULL;
            char *newValue = malloc(32);
            if (value != NULL) {
                if (*value == '\'') {
                    sprintf(newValue, "%s%s%s", "'\\'", value, "\\''");
                } else {
                    strcpy(newValue, value);
                }
            }

            char *path = (char *) malloc(1024);
            sprintf(path, "databases/%s/%s", currentDatabase, tableName);
            puts(currentDatabase);
            puts(tableName);
            puts(path);

            if (access(path, F_OK)) {
                send(client_sock, "ERROR : table didn't exist", MAX_BUFFER_SIZE, 0);
            } else {
                char *nColumns = malloc(64);
                char *nColumn = malloc(2);
                char *column = strtok(columns, "-");
                while (column != NULL) {
                    if (!strcmp(column, "*")) {
                        strcat(nColumns, "$0");
                        break;
                    }
                    nColumn = getNumOfColumn(path, column);

                    if (nColumns != NULL) {
                        strcat(nColumns, "$");
                        strcat(nColumns, nColumn);
                    } else {
                        send(client_sock, "ERROR : column didn't exist on the table", MAX_BUFFER_SIZE, 0);
                        continue;
                    }
                    column = strtok(NULL, "-");
                    if (column != NULL) strcat(nColumns, ",");
                }

                char *where = malloc(128);
                strcpy(where, "");
                if (columnComp != NULL) {
                    char *nColumnComp = getNumOfColumn(path, columnComp);

                    if (nColumnComp != NULL) {
                        sprintf(where, "NR == 1 || $%s ~ /^%s$/ ", nColumnComp, newValue);
                    } else {
                        send(client_sock, "ERROR : column didn't exist on the table", MAX_BUFFER_SIZE, 0);
                        continue;
                    }

                }

                bzero(command, MAX_BUFFER_SIZE);
                sprintf(command, "awk -F \"|\" '%s{print %s}' OFS=\"|\" %s", where, nColumns, path);
                puts(command);

                if (!(fpipe = (FILE*)popen(command, "r"))) exit(1);

                fseek(fpipe, 0, SEEK_SET);
                bzero(buffer, MAX_BUFFER_SIZE);
                fread(buffer, MAX_BUFFER_SIZE, 1, fpipe);
                puts(buffer);

                pclose(fpipe);
                send(client_sock, buffer, MAX_BUFFER_SIZE, 0);

            }
        } else if (!strcmp(buffer, "DUMP")) {
            bzero(buffer, MAX_BUFFER_SIZE);
            puts("got Here 1");

            char *path = malloc(128);
            strcpy(path, "databases/");
            strcat(path, currentDatabase);
            DIR* dir = opendir(path);
            struct dirent *de;
            char filename_qfd[100] ;

            if (dir) {
                puts("got Here 2");
                while ((de = readdir(dir)) != NULL) {

                    struct stat stbuf ;
                    sprintf( filename_qfd , "%s/%s",path,de->d_name) ;
                    if(stat(filename_qfd,&stbuf ) == -1) continue ;
                    if ( ( stbuf.st_mode & S_IFMT ) == S_IFDIR ) continue;
                    sprintf(buffer + strlen(buffer), "DROP TABLE %s;\n", de->d_name);
                    sprintf(buffer + strlen(buffer), "CREATE TABLE %s (", de->d_name);

                    char columns[1024];
                    puts("got Here 3");

                    FILE *table = fopen(filename_qfd, "r");

                    if (table == NULL) continue;

                    bzero(line, 128);
                    int nLine = 0;
                    while (fgets(line, 1024, table) != NULL) {
                        puts("got Here aaaaaaa");
//                puts(rtrim(rtrim(line, '\n'), '\r'));
                        nLine++;
                        strcpy(columns, rtrim(rtrim(line, '\n'), '\r'));

                        if (nLine == 1) {
                            char *column = strtok(columns, "|");
                            while (column != NULL) {
                                char *end;
                                char *columnName = strtok_r(column, "-", &end);
                                char *columnType = strtok_r(NULL, "-", &end);

                                sprintf(buffer + strlen(buffer), "%s %s", columnName, columnType);

                                column = strtok(NULL, "|");
                                if (column != NULL) strcat(buffer, ",");
                            }
                            strcat(buffer, ");\n\n");
                        } else {
                            sprintf(buffer + strlen(buffer), "INSERT INTO %s (", de->d_name);
                            bzero(columns, 1024);
                            strcpy(columns, rtrim(line, '\n'));

                            char *column = strtok(columns, "|");
                            while (column != NULL) {
                                strcat(buffer, column);
                                column = strtok(NULL, "|");
                                if (column != NULL) strcat(buffer, ",");
                            }
                            strcat(buffer, ");\n");
                            bzero(line, 128);
                        }
                    }
                    strcat(buffer, "\n");
                }
                puts("got Here 4");

                closedir(dir);

                send(client_sock, buffer, MAX_BUFFER_SIZE, 0);
            } else {
                send(client_sock, "ERROR : database didn't exist", MAX_BUFFER_SIZE, 0);
            }

        }

    }

    if (read_size == 0) {
        puts("Client disconnected");
        fflush(stdout);
    } else if (read_size == -1) {
        perror("RECV ERROR");
    }

    //Free the socket pointer
    free(socket_desc);

    pthread_exit(NULL);
}
