#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int MAX_BUFFER_SIZE = 1024;
int PORT = 5566;
char * DEFAULT_DATABASE_NAME = "default";
char * FORBIDDEN_CHARS = "`~!@#$%^&*()-=+\\|'\";:,<.>/?";
char * RESERVED_KEYWORDS = ",USE,CREATE,USER,DATABASE,TABLE,GRANT,"
                  "PERMISSION,INTO,DROP,COLUMN,FROM,SELECT,"
                  "INSERT,UPDATE,SET,DELETE,WHERE,";

int startWith(char *a, char *b) {
    if (strncmp(a, b, strlen(b)) == 0) return 1;
    return 0;
}

char *ltrim(char *s, char c) {
    while(*s == c) s++;
    return s;
}

char *rtrim(char *s, char c) {
    char* back = s + strlen(s);
    while(*--back == c);
    *(back+1) = '\0';
    return s;
}

char *trim(char *s, char c) {
    return rtrim(ltrim(s, c), c);
}

int isValid(char *keyword) {
    // Keyword cannot be NULL
    if (keyword == NULL) return 0;

    // Keyword must not contain any forbidden characters
    if (strpbrk(keyword, FORBIDDEN_CHARS)) return 0;

    // Keyword cannot be one of the reserved keywords
    char *resKey = strstr(RESERVED_KEYWORDS, keyword);
    if (strstr(RESERVED_KEYWORDS, keyword)) {
        if (*(resKey-1) == ',' && *(resKey+strlen(keyword)) == ',') return 0;
    }
    return 1;
}

int isKeyword(char *token, char *keyword) {
    if (token == NULL || strcmp(token, keyword) != 0) return 0;
    return 1;
}

char* validateUse(char *a) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(a, " ");               // USE

    token = strtok(NULL, " ");                  // [databaseName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);

    token = strtok(NULL, " ");                  // \0
    if (token != NULL) return NULL;

    char *ret = &args[0];
    return ret;
}

char* validateCreateUser(char *a) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(a, " ");               // CREATE
    token = strtok(NULL, " ");                  // USER

    token = strtok(NULL, " ");                  // [username]
    if (!isValid(token)) return NULL;
    strcpy(args, token);
    strcat(args, ",");

    token = strtok(NULL, " ");                  // IDENTIFIED
    if (!isKeyword(token, "IDENTIFIED")) return NULL;

    token = strtok(NULL, " ");                  // BY
    if (!isKeyword(token, "BY")) return NULL;

    token = strtok(NULL, " ");                  // [password]
    if (!isValid(token)) return NULL;
    strcat(args, token);

    token = strtok(NULL, " ");                  // \0
    if (token != NULL) return NULL;

    char *ret = &args[0];
    return ret;
}

char* validateGrantPermission(char *a) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(a, " ");               // GRANT
    token = strtok(NULL, " ");                  // PERMISSION

    token = strtok(NULL, " ");                  // [databaseName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);
    strcat(args, ",");

    token = strtok(NULL, " ");                  // INTO
    if (!isKeyword(token, "INTO")) return NULL;

    token = strtok(NULL, " ");                  // [username]
    if (!isValid(token)) return NULL;
    strcat(args, token);

    token = strtok(NULL, " ");                  // \0
    if (token != NULL) return NULL;

    char *ret = &args[0];
    return ret;
}

char* validateCreateDatabase(char *a) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(a, " ");               // CREATE
    token = strtok(NULL, " ");                  // DATABASE

    token = strtok(NULL, " ");                  // [databaseName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);

    token = strtok(NULL, "");                   // \0
    if (token != NULL) return NULL;

    char *ret = &args[0];
    return ret;
}

char* validateCreateTable(char *a) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(a, " ");               // CREATE
    token = strtok(NULL, " ");                  // TABLE

    token = strtok(NULL, " ");                  // [tableName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);

    token = strtok(NULL, "");                  // [column type, ...]
    if (token == NULL) return NULL;

    char *openBracket = strchr(token, '(');
    char *closeBracket = strrchr(token, ')');
    if (openBracket == NULL || closeBracket == NULL || closeBracket - openBracket <= 1) return NULL; // prevent no column

    char columns[1024];
    memset(columns, '\0', 1024);
    strncpy(columns, openBracket+1, closeBracket-openBracket-1);

    token = strtok(columns, ",");
    while (token != NULL) {
        char *end, *token2 = strtok_r(trim(token, ' '), " ", &end);
        if (!isValid(token2)) return NULL;
        strcat(args, ",");
        strcat(args, token2);

        token2 = strtok_r(NULL, " ", &end);
        if (strcmp(token2, "int") && strcmp(token2, "string")) return NULL;
        strcat(args, "-");
        strcat(args, token2);

        token2 = strtok_r(NULL, " ", &end);
        if (token2 != NULL) return NULL;

        token = strtok(NULL, ",");
    }

    char *ret = &args[0];
    return ret;
}

char* validateDropDatabase(char *a) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(a, " ");               // DROP
    token = strtok(NULL, " ");                  // DATABASE

    token = strtok(NULL, " ");                  // [databaseName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);

    token = strtok(NULL, "");                   // \0
    if (token != NULL) return NULL;

    char *ret = &args[0];
    return ret;
}

char* validateDropTable(char *a) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(a, " ");               // DROP
    token = strtok(NULL, " ");                  // TABLE

    token = strtok(NULL, " ");                  // [tableName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);

    token = strtok(NULL, "");                   // \0
    if (token != NULL) return NULL;

    char *ret = &args[0];
    return ret;
}

char* validateDropColumn(char *buffer) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(buffer, " ");          // DROP
    token = strtok(NULL, " ");                  // COLUMN

    token = strtok(NULL, " ");                  // [columnName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);
    strcat(args, ",");

    token = strtok(NULL, " ");                  // FROM
    if (!isKeyword(token, "FROM")) return NULL;

    token = strtok(NULL, " ");                  // [tableName]
    if (!isValid(token)) return NULL;
    strcat(args, token);

    token = strtok(NULL, " ");                  // \0
    if (token != NULL) return NULL;

    char *ret = &args[0];
    return ret;
}

char* validateInsertInto(char *buffer) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(buffer, " ");          // INSERT
    token = strtok(NULL, " ");                  // INTO

    token = strtok(NULL, " ");                  // [tableName]
    if (!isValid(token)) return NULL;
    strcpy(args, token);

    token = strtok(NULL, "");                   // [column1, ...]
    if (token == NULL) return NULL;

    char *openBracket = strchr(token, '(');
    char *closeBracket = strrchr(token, ')');
    if (openBracket == NULL || closeBracket == NULL || closeBracket - openBracket <= 1) return NULL; // prevent no column

    char columns[1024], nextChar, column[32];
    memset(columns, '\0', 1024);
    strncpy(columns, openBracket+1, closeBracket-openBracket-1);

    int colIndex = 0;
    char *currentPos = &columns[colIndex];

    while(*currentPos != '\0') {
        strcat(args, ",");
        memset(column, '\0', 32);
        if (*currentPos == ' ') currentPos++;
        if (*currentPos == '\'') nextChar = '\'';
        else nextChar = ',';

        char *x = strchr(currentPos+1, nextChar);
        if (x != NULL) {
            strncat(args, currentPos, x - currentPos + (*currentPos == '\''));


            colIndex = x - currentPos + 1 + (*currentPos == '\'');
            currentPos += colIndex;
        } else {
            strcat(args, currentPos);
            currentPos += strlen(currentPos);
        }
    }

    char *ret = &args[0];
    return ret;
}

char* validateUpdate(char *buffer) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(buffer, " ");          // UPDATE

    token = strtok(NULL, " ");                  // [tableName]
    if (token == NULL || strpbrk(token, ",")) return NULL;
    strcpy(args, token);
    strcat(args, "|");

    token = strtok(NULL, " ");                  // SET
    if (token == NULL || strcmp(token, "SET") != 0) return NULL;

    token = strtok(NULL, "=");                  // [columnName]
    if (token == NULL || strpbrk(token, ",")) return NULL;
    strcat(args, trim(token, ' '));
    strcat(args, "|");

    token = strtok(NULL, "");                  // [value]
    if (token == NULL) return NULL;

    if (*token == ' ') token++;
    char columnValue[32];
    memset(columnValue, '\0', 32);
    char nextChar = *token == '\'' ? '\'' : ' ';
    char *x = strchr(token+1, nextChar);

    if (nextChar == '\'' && x == NULL) return NULL;


    if (nextChar == ' ' && x == NULL) {
        strcpy(columnValue, token);
        token += strlen(token);
    } else {
        strncpy(columnValue, token, x-token+(nextChar=='\''));
        token += x-token+(nextChar=='\'');
        if (strpbrk(columnValue, "|")) return NULL;
    }

    strcat(args, columnValue);

    token = strtok(token, " ");                  // WHERE ?
    if (token != NULL) {
        if (strcmp(token, "WHERE") != 0) return NULL;
        strcat(args, "|");

        token = strtok(NULL, "=");                  // [columnName]
        if (token == NULL || strpbrk(token, ",")) return NULL;
        strcat(args, trim(token, ' '));
        strcat(args, "|");

        token = strtok(NULL, "=");                  // [value]
        if (token == NULL || strpbrk(token, "|")) return NULL;
        strcat(args, trim(token, ' '));
    }

    char *ret = &args[0];
    return ret;
}

char* validateDeleteFrom(char *buffer) {
    char args[1024];
    memset(args, '\0', 1024);

    char *token = strtok(buffer, " ");          // DELETE
    token = strtok(NULL, " ");                  // FROM

    token = strtok(NULL, " ");                  // [tableName]
    if (token == NULL || strpbrk(token, ",")) return NULL;
    strcpy(args, token);

    token = strtok(NULL, " ");                  // WHERE ?
    if (token != NULL) {
        if (strcmp(token, "WHERE") != 0) return NULL;
        strcat(args, "|");

        token = strtok(NULL, "=");                  // [columnName]
        if (token == NULL || strpbrk(token, "|")) return NULL;
        strcat(args, trim(token, ' '));
        strcat(args, "|");

        token = strtok(NULL, "=");                  // [value]
        if (token == NULL || strpbrk(token, "|")) return NULL;
        strcat(args, trim(token, ' '));
    }

    char *ret = &args[0];
    return ret;
}

char* validateSelect(char *buffer) {
    char args[1024];
    memset(args, '\0', 1024);
    strcpy(args, "");

    char *token = strtok(buffer, " ");          // SELECT

    token = strtok(NULL, "");

    char columns[1024];
    memset(columns, '\0', 1024);

    char *from = strstr(token, "FROM");
    strncpy(columns, token, from-token);

    char *column = strtok(columns, ",");
    if (column == NULL || strpbrk(column, "-")) return NULL;
    strcat(args, trim(column, ' '));

    column = strtok(NULL, ",");
    while (column != NULL) {
        if (strpbrk(column, "-")) return NULL;
        strcat(args, "-");
        strcat(args, trim(column, ' '));
        column = strtok(NULL, ",");
    }
    strcat(args, "|");

    strcpy(token, from+5);
    token = strtok(token, " ");                 // [tableName]
    if (token == NULL || strpbrk(token, "|")) return NULL;
    strcat(args, token);

    token = strtok(NULL, " ");                  // WHERE ?
    if (token != NULL) {
        if (strcmp(token, "WHERE") != 0) return NULL;
        strcat(args, "|");

        token = strtok(NULL, "=");              // [columnName]
        if (token == NULL || strpbrk(token, ",")) return NULL;
        strcat(args, trim(token, ' '));
        strcat(args, "|");

        token = strtok(NULL, "=");              // [value]
        if (token == NULL || strpbrk(token, "|")) return NULL;
        token = trim(token, ' ');
        if (*(token) == '\'' && *(token + strlen(token) - 1) != '\'') return NULL;
        if (*(token) != '\'' && *(token + strlen(token) - 1) == '\'') return NULL;
        strcat(args, trim(token, ' '));
    }

    char *ret = &args[0];
    return ret;
}

int main(int argc, char* argv[]){
    char *username = NULL, *password = NULL;

    if (argc != 1 && argc != 5) {
        puts("HMMMMM SUS");
        exit(1);
    } else if (argc == 5) {
        if (!strcmp(argv[1], "-u")) username = strdup(argv[2]);
        if (!strcmp(argv[3], "-p")) password = strdup(argv[4]);
    }

    if (!getuid()) {
        username = strdup("root");
        password = strdup("root");
    }

    char *ip = "127.0.0.1";

    int sock;
    struct sockaddr_in addr;
    char buffer[MAX_BUFFER_SIZE], query[MAX_BUFFER_SIZE];

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0){
        perror("[-]Socket error");
        exit(1);
    }

    memset(&addr, '\0', sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = PORT;
    addr.sin_addr.s_addr = inet_addr(ip);

    if(connect(sock, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
        perror("Error");
        exit(1);
    }

    printf("Logging in\n");
    if (!getuid()) {
        username = strdup("root");
        password = strdup("root");
    } else if (username == NULL || password == NULL) {
        username = strdup("");
        password = strdup("");

        printf("Username : ");scanf("%s", username);
        printf("Password : ");scanf("%s", password);

        getchar();
    }

    // START LOGGING IN
    bzero(buffer, MAX_BUFFER_SIZE);
    strcpy(buffer, username);
    strcat(buffer, " ");
    strcat(buffer, password);
    send(sock, buffer, MAX_BUFFER_SIZE, 0);

    bzero(buffer, 1024);
    recv(sock, buffer, sizeof(buffer), 0);

    if (strcmp(buffer, "SUCCESS") != 0) {
        puts("ERROR login");
        exit(1);
    }
    // FINISH LOGGING IN

    printf("Connected to the database server as %s.\n", username);
    printf("Current database : %s\n", DEFAULT_DATABASE_NAME);

    while (1) {
        printf("> ");
        bzero(buffer, MAX_BUFFER_SIZE);
        fgets(buffer, MAX_BUFFER_SIZE, stdin);
        if (startWith(buffer, "exit")) break;
        strcpy(query, buffer);

        if (buffer[strlen(buffer) - 2] == ';') {
            buffer[strlen(buffer) - 2] = '\0';
            if (startWith(buffer, "USE") == 1) {
                char *args = validateUse(buffer);
                if(args != NULL) {
                    strcpy(buffer, "USE,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) printf("Current database changed to %s\n", args);
                    else puts(buffer);
                } else {
                    puts("ERROR 'USE' SYNTAX");
                }
            } else if (startWith(buffer, "CREATE USER") == 1) {
                char *args = validateCreateUser(buffer);
                if (args != NULL) {
                    strcpy(buffer, "CREATE USER,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) printf("New user created : %s\n", strtok(args, ","));
                    else puts(buffer);
                } else {
                    puts("ERROR 'CREATE USER' SYNTAX");
                }
            } else if (startWith(buffer, "GRANT PERMISSION") == 1) {
                char *args = validateGrantPermission(buffer);
                if (args != NULL) {
                    strcpy(buffer, "GRANT PERMISSION,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) puts("Success grant permission");
                    else puts(buffer);
                } else {
                    puts("ERROR 'GRANT PERMISSION' SYNTAX");
                }
            } else if (startWith(buffer, "CREATE DATABASE") == 1) {
                char *args = validateCreateDatabase(buffer);
                if (args != NULL) {
                    strcpy(buffer, "CREATE DATABASE,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) puts("Database created");
                    else puts(buffer);
                } else {
                    puts("ERROR 'CREATE DATABASE' SYNTAX");
                }
            } else if (startWith(buffer, "CREATE TABLE") == 1) {
                char *args = validateCreateTable(buffer);
                if (args != NULL) {
                    strcpy(buffer, "CREATE TABLE,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) puts("Table created");
                    else puts(buffer);
                } else {
                    puts("ERROR 'CREATE TABLE' SYNTAX");
                }
            } else if (startWith(buffer, "DROP DATABASE") == 1) {
                char *args = validateDropDatabase(buffer);
                if (args != NULL) {
                    strcpy(buffer, "DROP DATABASE,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) puts("Database dropped");
                    else puts(buffer);
                } else {
                    puts("ERROR 'DROP DATABASE' SYNTAX");
                }
            } else if (startWith(buffer, "DROP TABLE") == 1) {
                char *args = validateDropTable(buffer);
                if (args != NULL) {
                    strcpy(buffer, "DROP TABLE,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) puts("table dropped");
                    else puts(buffer);
                } else {
                    puts("ERROR 'DROP TABLE' SYNTAX");
                }
            } else if (startWith(buffer, "DROP COLUMN") == 1) {
                char *args = validateDropColumn(buffer);
                if (args != NULL) {
                    strcpy(buffer, "DROP COLUMN,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) puts("Column dropped");
                    else puts(buffer);
                } else {
                    puts("ERROR 'DROP COLUMN' SYNTAX");
                }
            } else if (startWith(buffer, "INSERT INTO") == 1) {
                char *args = validateInsertInto(buffer);
                if (args != NULL) {
                    strcpy(buffer, "INSERT INTO,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    if (!strcmp(buffer, "SUCCESS")) puts("Data inserted");
                    else puts(buffer);
                } else {
                    puts("ERROR 'INSERT INTO' SYNTAX");
                }
            } else if (startWith(buffer, "UPDATE") == 1) {
                char *args = validateUpdate(buffer);
                if (args != NULL) {
                    strcpy(buffer, "UPDATE,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    puts(args);

                    if (!strcmp(buffer, "SUCCESS")) puts("Data updated");
                    else puts(buffer);
                } else {
                    puts("ERROR 'UPDATE' SYNTAX");
                }
            } else if (startWith(buffer, "DELETE FROM") == 1) {
                char *args = validateDeleteFrom(buffer);
                if (args != NULL) {
                    strcpy(buffer, "DELETE FROM,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    puts(args);

                    if (!strcmp(buffer, "SUCCESS")) puts("Data updated");
                    else puts(buffer);
                } else {
                    puts("ERROR 'DELETE FROM' SYNTAX");
                }
            } else if (startWith(buffer, "SELECT") == 1) {
                char *args = validateSelect(buffer);
                if (args != NULL) {
                    strcpy(buffer, "SELECT,");
                    strcat(buffer, args);
                    sprintf(buffer + strlen(buffer), "~%s", query);
                    send(sock, buffer, MAX_BUFFER_SIZE, 0);

                    bzero(buffer, MAX_BUFFER_SIZE);
                    recv(sock, buffer, MAX_BUFFER_SIZE, 0);

                    puts(buffer);
                } else {
                    puts("ERROR 'SELECT' SYNTAX");
                }
            } else {
                puts("The command is not recognized");
            }
        } else {
            puts("Statement must be closed with semicolon (;)");
        }
    }

    close(sock);
    printf("Disconnected from the server.\n");

    return 0;

}

